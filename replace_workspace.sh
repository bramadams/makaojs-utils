#!/bin/bash

if [ "$#" -ne "3" ] ; then
  echo "Usage: $0 file makaows beews"
  exit
fi

FILE=$1
MAKAO_WS=$2
BEE_WS=$3


MAKAO=`cat $MAKAO_WS`
BEE=`cat $BEE_WS`

echo "Replacing ${BEE} by ${MAKAO} as workspace"
sed -i "s:${BEE}:${MAKAO}:g" $FILE

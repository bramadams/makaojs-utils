#!/bin/bash

if [ "$#" -lt "1" ] ; then
  echo "Usage: $0 target_name"
  echo ""
  echo "=> Specify #make processes (default: 1) using: NRJ=4 $0 target_name"
  echo "=> Specify timing command (default: none) using: TIMH=/usr/bin/time $0 target_name"
  exit
fi

TARGET=$1

NRJ=${NRJ:-1}
TIMH=${TIMH:-}
MAKE=${MAKE:-make}

OLD_SHELL=$SHELL

#quoting!!!
SHELL_QUOTED=`echo "${TIMH} ${OLD_SHELL} -o xtrace" | sed 's/ /\\\\ /g'`
export SHELL=${TIMH}\ ${OLD_SHELL}\ -o\ xtrace
export CONFIG_SHELL=${TIMH}\ ${OLD_SHELL}\ -o\ xtrace

#MAKEFLAGS is automatically set by GNU Make!
${MAKE} -j "${NRJ}" -w --debug=v --debug=m -p SHELL:="${SHELL}" CONFIG_SHELL:="${SHELL}" MAKE="${MAKE} -w --debug=v --debug=m -p SHELL:=${SHELL_QUOTED}" $TARGET

#!/bin/bash

if [ "$#" -ne "1" ] ; then
  echo "Usage: $0 target"
  exit
fi

TARGET=$1

echo "Running MAKAO"
./makewrapper.sh $TARGET 2&> makaotrace.txt

echo "Locating workspace"
#grep -Pe "^#\s+make:\s+Leaving\s+directory\s+" makaotrace.txt | tail -1 | perl -pe "s/^.*'(.*)'.*$/\1/" > makaoworkspace.txt
grep -Pe "^\s*CURDIR\s+:=\s+" makaotrace.txt | tail -1 | perl -pe "s/^\s*CURDIR\s+:=\s+(.*)$/\1/" > makaoworkspace.txt

echo "Parsing MAKAO trace"
./generate_makao_graph.pl -in makaotrace.txt -out makaotrace.gdf -format gdf

echo "Zipping trace files"
gzip makaotrace.gdf
gzip makaotrace.gdf.xml
#rm -f makaotrace.txt

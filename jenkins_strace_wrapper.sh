#!/bin/bash

if [ "$#" -ne "1" ] ; then
  echo "Usage: $0 target"
  exit
fi

TARGET=$1


echo "Running strace"
strace -v -f -o beetrace.strace make $TARGET

echo "Locating workspace"
grep -Pe "^\d+\s+chdir\(\"/var/lib/jenkins/" beetrace.strace | tail -1 | perl -pe "s/^.*\"(.*)\".*$/\1/" > beeworkspace.txt

